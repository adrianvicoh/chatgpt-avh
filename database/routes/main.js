import express from 'express';
import { test } from '../controllers/controller.js';
import * as messageController from '../controllers/messages.js';

var routes = express.Router();

// Index 
routes.get("/", test, async (req, res, next) => {
  res.render('index', req.result);
});

// Store messages
routes.post("/store", async (req, res, next) => {
  var newMessage = await messageController.storeMessage(req);
  res.status(200).json(newMessage);
});

// Get messages
routes.post("/get", async (req, res, next) => {
  var messages = await messageController.getMessages(req);
  res.status(200).json(messages);
});

export { routes }