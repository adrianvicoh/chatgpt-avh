import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var MessageSchema = new Schema({
    user: {type: String, required: true},
    message: {type: String, required: true},
    room: {type: String, required: true},
    timestamp: {type: Date, required: true, default: Date.now()}
});

const Message = mongoose.model('Message', MessageSchema);

export { Message }