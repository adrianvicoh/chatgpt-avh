import { Configuration, OpenAIApi } from "openai";

export async function request(req, res) {
    try {
        const configuration = new Configuration({
            apiKey: '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',
          });
          const openai = new OpenAIApi(configuration);
          const question=req.body.data.message;

          const response = await openai.createCompletion({
            model: "text-davinci-003",
            prompt: question,
            temperature: 0.9,
            max_tokens: 15,
            top_p: 1,
            frequency_penalty: 0.0,
            presence_penalty: 0.6,
            stop: [" Human:", " AI:"],
          });

          let respuestaFinal = response.data.choices[0].text;

          //console.log(respuestaFinal);
          return respuestaFinal;
          
    } catch (error) {
        res.status(400).json({'error':`Error sending request ${error}`});
        console.error(`Error sending request ${error}`);
    }
}


