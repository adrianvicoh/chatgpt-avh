// Aquí va todo el código de las acciones del servidor

//io.js
const socketPort =  2000;
//const { Server } = require("socket.io");
import { Server } from "socket.io";
//const socketServer = require("http").createServer();
import http from "http";

const socketServer = http.createServer();

socketServer.listen(socketPort, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${socketPort}`);
});

export const io = new Server(socketServer, {
    cors: {
        origin: "http://localhost:3000", //Esta será la dirección de vuestra web
    },
});

function storeMessage(data) {
  // Llamar ruta para almacenar el mensaje
  fetch(`http://databaseAPI:4000/store`, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({ data }),
  })
    .then((response) => {
      console.log(response.user);
    })
    .catch(function (err) {
      console.log("Unable to fetch -", err);
    });
}

//-------------CONEXIONES DEL CHAT-------------||

// Enviar mensaje de broadcast a todos los clientes conectados
io.on("connection", (socket) => {
  var data = {msg: "Bienvenido al chat"}
  socket.emit("connected", data ,(response) => {
    console.log(response); //"Recibido!" Respuesta enviada por el cliente
  });
  //Este socket tendrá un atributo user con valor "User",
  //desde la primera conexión hasta que se cierre el socket
  if (!socket.user) socket.user = "User";

  // Entrar en sala 1 por defecto, si no está definida
  if (!socket.room) socket.room = "sala1"
  socket.join(socket.room);

  //Todos los clientes que estén escuchando el evento "toChat" recibiran el mensaje enviado por el cliente que lanzó el mensaje
  socket.on("toChat", (data) => {
    io.to(socket.room).emit("toChat", data);
    data.room = socket.room;
    storeMessage(data);

      // Si está activada la opción de GPT
      if(data.gpt) {
        // Llamar a la ruta para enviar el mensaje
        fetch(`http://gptAPI:5000/request`, {
          mode: "cors",
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({ data }),
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Error en la solicitud');
            }
          })
          .then((gptResponse) => {
            console.log(gptResponse);
            // Pasar a la variable de respuesta los mensajes
            var data = {user: "GPT", message: gptResponse.response};
            data.room = socket.room;
            storeMessage(data);
            io.to(socket.room).emit("toChat", data);
          })
          .catch(function (err) {
            console.log("Unable to fetch -", err);
          });

    } 

  });

  // Acciones para entrar y salir de las salas
  socket.on("joinRoom", (data) => {
    console.log(data.user + " ha abandonado la sala " + socket.room);
    socket.leave(socket.room);
    socket.join(data.room);
    // Se puede definir cualquier variable dentro del socket, que se queda registrada con cada conexión de servidor
    socket.room = data.room;
    socket.user = data.user;
    socket.gpt = data.gpt;

    // Obtener todos los mensajes de la sala a la que se une el usuario
    fetch(`http://databaseAPI:4000/get`, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ 'room' : socket.room }),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Error en la solicitud');
        }
      })
      .then((roomMessages) => {
        // Pasar a la variable de respuesta los mensajes
        var msg = socket.user + " se ha unido a la sala " + socket.room;
        var data = {user: "server", roomMessages: roomMessages, msg: msg};
        io.to(socket.room).emit("joinRoom", data);
      })
      .catch(function (err) {
        console.log("Unable to fetch -", err);
      });
    
  });

});