import express from 'express';
import { test } from '../controllers/controller.js';
import { io } from '../io.js';

var routes = express.Router();

// Index 
routes.get("/", test, async (req, res, next) => {
  res.render('index', req.result);
});

export { routes }

