import express from 'express';
import { test } from '../controllers/controller.js';
import * as requestGPT from '../controllers/requestGPT.js';

var routes = express.Router();

// Index 
routes.get("/", async (req, res, next) => {
  res.render('index', req.result);
});

routes.post("/request", async (req, res, next) => {
  var gptResponse = await requestGPT.request(req, res);

  res.status(200).json({response:gptResponse});
});

export { routes }