// Aquí va todo el código con las acciones del cliente

//Cuando todos los elementos de nuestra web se hayan cargado:
document.addEventListener("DOMContentLoaded", function (event) {
    //Nos conectamos al servicio de socket.
    //La constante socket será nuestro vinculo con el servidor donde definiremos todas las accioens
    const socket = io("http://localhost:2000");
  
      /*
     * Acciones que se realizan cuando se detecta el evento "connected" lanzado por el servidor
     */
    socket.on("connected", (data,callback) => {
      console.log(data.msg);  //data enviada por el servidor al recibir la conexión
      callback("Recibido!");  //El servidor recibirá "Recibido!"
      socket.emit();
    });

    //Accion a realizar cuando se da al boton de enviar
    document.getElementById("send").addEventListener("click", (e) => {
        //Obtenemos todos los elementos del formulario para trabajar con ellos
        e.preventDefault();
        var msgInput = document.getElementById("msg");
        var usr = document.getElementById("user").value;
        var msg = msgInput.value;
        var gptActive = document.getElementById("gpt").checked;

        // Definimos el mensaje que vamos a enviar
        var toSend = { user: usr, message: msg, gpt: gptActive };

        //Enviamos el mensaje al servidor utilizando el evento "broadcast" definido por nosotros
        socket.emit("toChat", toSend);

    });

    // Imprimir el mensaje que recibe del servidor mediante broadcast
    socket.on("toChat", (data) => {
        var chatBox = document.getElementById("chat");
        chatBox.value += `${data.user}: ${data.message}\n`;
    });

    // Cambios de sala1
    document.getElementById("sala1").addEventListener("click", (e) => {
      e.preventDefault();
      var usr = document.getElementById("user").value;
      var toSend = { user: usr, room: "sala1" };
      socket.emit("joinRoom", toSend);
    });

    // Cambios de sala2
    document.getElementById("sala2").addEventListener("click", (e) => {
      e.preventDefault();
      var usr = document.getElementById("user").value;
      var toSend = { user: usr, room: "sala2" };
      socket.emit("joinRoom", toSend);
    });

    // Respuesta del servidor si alguien entra a la sala
    socket.on("joinRoom", (data) => {
      var chatBox = document.getElementById("chat");
      chatBox.value = '';
      // Mostrar todos los mensajes almacenados de esa sala
      for(let i = 0; i < data.roomMessages.length; i++) {
        chatBox.value += data.roomMessages[i].user + ": " + data.roomMessages[i].message + "\n";
      }
      chatBox.value += data.user + ": " + data.msg + "\n";
  });
  
});
