import {Message} from "../models/message.js";

export async function storeMessage(req, res) {
    try {
        var message = new Message(req.body.data);
        message.save();
        return message;
    } catch (error) {
        res.status(400).json({'errir':`Error saving Message ${error}`});
        console.error(`Error saving Message ${error}`);
    }
}

export async function getMessages(req) {
    try {
        return await Message.find({room: req.body.room}, function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Messages" ${error} from ${req.body.room}`);
    }

}